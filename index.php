<?php 
include('jd.php');
$jd = new jd();
$jd::openhtml();
$jd::openhead();
$jd::closehead();
$body="";
$jd::openbody($body);
//tablas
$class="table";
//$class="table table-striped";
//$class="table table-bordered";
//$class="table table-hover";
//$class="table table-condensed";

$jd::opentable($class);
//abro fila 

//$rowclass="active";
//$rowclass="success";
$rowclass="warning";
//$rowclass="danger";

//$colclass="active";
$colclass="success";
//$colclass="warning";
//$colclass="danger";

//openrow abre fila
$jd::openrow();
$datocolumna1="a";
$datocolumna2="b";
$jd::opencol($datocolumna1);//abrocolumna
$jd::closecol();//cierrocolumna
$jd::opencol($datocolumna2,$colclass);
$jd::closecol();
$jd::closerow();
//cierro fila 

$jd::openrow($rowclass);
$datocolumna1="c";
$datocolumna2="d";
$jd::opencol($datocolumna1);//abrocolumna
$jd::closecol();//cierrocolumna
$jd::opencol($datocolumna2);
$jd::closecol();
$jd::closerow();

$jd::closetable();
//cierro tabla 

//labels 
$labeldato="label";
$jd::openlabel($labeldato);
$jd::closelabel();
$jd::br();
$jd::br();//saltodelinea
//icon
//http://getbootstrap.com/components/ 
// 
// Glyphicons
// Available glyphs

$iconclass="glyphicon glyphicon-search";

$jd::openicon($iconclass);
$jd::closeicon();
$jd::br();
$jd::br();

$buttonclass="btn btn-default";
$jd::openbutton("boton",$buttonclass);
$jd::closebutton();

//dropdownbutton

$list=array("Action","Another action","Something else here","/","Separated link");
$jd::opendropdownbutton($list);
$jd::closedropdownbutton();

$jd::br();
$jd::br();

//imput
$value="$";
$jd::openinput($value);
$jd::closeinput();

//tabs
$jd::br();
$jd::br();
$jd::opentabs();
$jd::closetabs();

$jd::Badge("Correo","42");

$jd::br();
$jd::br();


$tipoalert="success";
//$tipoalert="info";
//$tipoalert="warning";
//$tipoalert="danger";

$jd::alert($tipoalert,"mi alerta");
        
$jd::openDismissablealert($tipoalert,"mi alerta");
$jd::closeDismissablealert();

$jd::openprogressbar(80);//80%
$jd::closeprogressbar();

//panel 
$jd::panel("danger","contenido");

$jd::modal("mymodal");


$jd::br();
$jd::br();

$jd::tooltip("tooltip","contenido tooltip");

$jd::br();
$jd::br();



$jd::br();
$jd::br();

$jd::ajax("midiv","loadajax","Let AJAX change this text","ajax_info.txt");

$jd::closebody();
$jd::closehtml();

?>
